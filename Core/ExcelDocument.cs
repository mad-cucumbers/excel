﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Core.Properties;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table.PivotTable;

namespace Core
{
    public class ExcelDocument
    {
        private readonly ExcelPackage _package;
        private readonly ExcelWorkbook _workbook;
        public DefinedNames DefinedNames { get; private set; }
        private Dictionary<string, Dictionary<int, double>> RowFixedHeights { get; set; }
        private Dictionary<string, Dictionary<int, double>> ColFixedWidths { get; set; }
        private Dictionary<string, Font> RangeFonts { get; set; }
        private Dictionary<(int, int), int> PageBreakColMap { get; set; }
        private Dictionary<(int, int), int> PageBreakRowMap { get; set; }
        private decimal MaximumDigitWidth
        {
            get => _workbook.MaxFontWidth;
        }
        private Graphics Graphics { get; set; }
        private ExcelWorksheet CurrentWorksheet { get; set; }
        private ExcelRange CurrentWorkSheetCells { get; set; }
        public int CurrentWorksheetNum { get; private set; }
        private string CurrentWorkSheetName { get; set; }
        private int CurrentPageNumRow { get; set; }
        private int CurrentPageNumCol { get; set; }
        public int WorkSheetsCount => _workbook.Worksheets.Count;
        public Stream Stream => _package.Stream;
        private IEnumerable<string> TemplateSheets { get; set; }
        private Stream _outputDocumentStream;

        private const int pointsPerInch = 72;
        private const string missingRef = "REF!";

        public ExcelDocument(Stream Document, IEnumerable<string> TemplateSheets)
        {
            _package = new ExcelPackage(Document);
            _workbook = _package.Workbook;
            this.TemplateSheets = TemplateSheets;
            DefinedNames = new DefinedNames(GetDefinedNamesMap());
        }

        public void AddColumnToPivotTable(string PivotTableName, dynamic PivotWorkSheetId, string PivotFieldName, eSortType AscDescSorting = eSortType.None)
        {
            ExcelPivotTable pivotTable = GetWorksheet(PivotWorkSheetId).PivotTables[PivotTableName];
            ExcelPivotTableFieldCollection fieldsCollection = pivotTable.Fields;
            ExcelPivotTableRowColumnFieldCollection columnCollection = pivotTable.ColumnFields;
            ExcelPivotTableField pivotField = columnCollection.Add(fieldsCollection[PivotFieldName]);

            pivotField.SubTotalFunctions = eSubTotalFunctions.None;
            pivotField.Sort = AscDescSorting;
        }

        public void AddRangeRichText(string Bookmark, string Text, bool NeedClear = false, bool Bold = false, bool Italic = false, bool Underline = false)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            ExcelRange range = GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol];
            range.IsRichText = true;
            ExcelRichTextCollection richTextCollection = range.RichText;

            if(NeedClear)
            {
                richTextCollection.Clear();
            }

            ExcelRichText richText = richTextCollection.Add(Text);
            richText.Bold = Bold;
            richText.Italic = Italic;
            richText.UnderLine = Underline;
        }

        public void AddRowToPivotTable(string PivotTableName, dynamic PivotWorkSheetId, string PivotFieldName, eSortType AscDescSorting = eSortType.None)
        {
            ExcelPivotTable pivotTable = GetWorksheet(PivotWorkSheetId).PivotTables[PivotTableName];
            ExcelPivotTableFieldCollection fieldsCollection = pivotTable.Fields;
            ExcelPivotTableRowColumnFieldCollection rowCollection = pivotTable.RowFields;
            ExcelPivotTableField pivotField = rowCollection.Add(fieldsCollection[PivotFieldName]);
            pivotField.SubTotalFunctions = eSubTotalFunctions.None;
            pivotField.Sort = AscDescSorting;
        }

        public void ClearRange(string Bookmark)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol].Clear();
        }

        public Stream CloseDocument(bool Save = false)
        {
            if (Graphics != null)
            {
                Graphics.Dispose();
            }

            _outputDocumentStream = new MemoryStream();

            if (Save)
            {
                SaveAs(_outputDocumentStream);
                _outputDocumentStream.Position = 0;
                //Stream stream = File.Create(@"C:\Temp\Done.xlsx");
                //_outputDocumentStream.Close();
                //stream.Close();
                //_outputDocumentStream = stream;
            }
            else
            {
                _package.Dispose();
            }

            return _outputDocumentStream;
        }

        public int ColCount(int WorkSheetNum) => GetWorksheet(WorkSheetNum).Dimension.End.Column;

        public void CopyRange(string Bookmark, string destinationAddr, bool clear = false)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            ExcelRange cellsFrom = GetWorksheet(worksheetName).Cells;
            ExcelRange rangeFrom = cellsFrom[fromRow, fromCol, toRow, toCol];

            ExcelRange rangeTo = CurrentWorkSheetCells[destinationAddr];
            ExcelCellAddress cellAddressTo = rangeTo.Start;
            int destinationRow = cellAddressTo.Row;
        
            rangeFrom.Copy(rangeTo);
        
            if (GetWorksheet(worksheetName).Index != CurrentWorksheetNum ||
                destinationRow > toRow ||
                destinationRow + (toRow - fromRow) < fromRow)
            {
                for (int i = fromRow; i <= toRow; i++)
                {
                    ExcelRow excelRow = CurrentWorksheet.Row(i);
                    double heightDouble = excelRow.Height;
                    int row = destinationRow + i - fromRow;
                    excelRow = CurrentWorksheet.Row(row);
                    excelRow.Height = heightDouble;
                }
            }
        
            if (clear)
            {
                cellsFrom.Clear();
            }
        }

        public void DeleteColumn(int ColIndex, dynamic Worksheet) => GetWorksheet(Worksheet).DeleteColumn(ColIndex);

        public void DeleteColumnsByName(string BookmarkName, dynamic WorkSheet)
        {
            int i;
            var (_, _, fromCol, _, toCol) = GetNamedRangeAddress(BookmarkName);
        
            for (i = toCol; i >= fromCol; i--)
            {
                DeleteColumn(i, WorkSheet);
            }
        }

        public void DeleteRow(int RowIndex, int RowCnt, int WorkSheet) => GetWorksheet(WorkSheet).DeleteRow(RowIndex, RowCnt);

        public void DeleteWorkSheet(int id) => _workbook.Worksheets.Delete(id);
        public void DeleteWorkSheet(string name) => _workbook.Worksheets.Delete(name);

        public ExcelWorksheet GetCurrentWorksheet() => _workbook.Worksheets[CurrentWorksheetNum];

        private Dictionary<(string, string), (int, int, int, int)> GetMergedCells()
        {
            Dictionary<(string, string), (int, int, int, int)> mergedCellsMap = new();
        
            foreach(string sheetName in TemplateSheets)
            {
                ExcelWorksheet workSheet = GetWorksheet(sheetName);

                foreach(var mergetCellName in workSheet.MergedCells)
                {
                    ExcelAddress excelAddress = new(mergetCellName);

                    mergedCellsMap.Add((excelAddress.Start.Address, workSheet.Name), (excelAddress.Start.Row, excelAddress.Start.Column, excelAddress.End.Row, excelAddress.End.Column));
                }
            }
        
            return mergedCellsMap;
        }

        public (string, int, int, int, int) GetNamedRangeAddress(string Bookmark) => DefinedNames[Bookmark];

        private Font GetNamedRangeFont(string Bookmark, bool ForceObtain = false)
        {
            Font font;
        
            if (RangeFonts == null)
            {
                RangeFonts = new Dictionary<string, Font>();
            }
        
            if (!ForceObtain && RangeFonts.ContainsKey(Bookmark))
            {
                font = RangeFonts[Bookmark];
            }
            else
            {
                var (worksheetName, fromRow, fromCol, _, _) = DefinedNames[Bookmark];
                ExcelWorksheet worksheet = GetWorksheet(worksheetName);
                ExcelStyle style = worksheet.Cells[fromRow, fromCol].Style;
                FontStyle fontStyle = FontStyle.Regular;
                if (style.Font.Bold && !style.Font.Italic)
                {
                    fontStyle = FontStyle.Regular | FontStyle.Bold;
                }

                if (!style.Font.Bold && style.Font.Italic)
                {
                    fontStyle = FontStyle.Regular | FontStyle.Italic;
                }

                if (style.Font.Bold && style.Font.Italic)
                {
                    fontStyle = FontStyle.Regular | FontStyle.Bold | FontStyle.Italic;
                }

                font = new Font(style.Font.Name, style.Font.Size, fontStyle);
        
                RangeFonts.Add(Bookmark, font);
            }
        
            return font;
        }

        public float GetNamedRangeFontSize(string Bookmark)
        {        
            var (worksheetName, fromRow, fromCol, _, _) = DefinedNames[Bookmark];
        
            return GetWorksheet(worksheetName).Cells[fromRow, fromCol].Style.Font.Size;
        }

        public double GetNamedRangeHeightFixed(string Bookmark, bool FillMapCustomHeights = false)
        {
            double ret = 0;
            Dictionary<int, double>  bookmarkRowHeights = null;

            var (worksheetName, fromRow, _, toRow, _) = DefinedNames[Bookmark];

            if (FillMapCustomHeights)
            {
                if (RowFixedHeights == null)
                {
                    RowFixedHeights = new Dictionary<string, Dictionary<int, double>>();
                }

                if (RowFixedHeights.ContainsKey(Bookmark))
                {
                    FillMapCustomHeights = false;
                }
                else
                {
                    bookmarkRowHeights = new Dictionary<int, double>();
                }
            }

            ExcelWorksheet worksheet = GetWorksheet(worksheetName);
            for (int curRow = fromRow; curRow <= toRow; curRow++)
            {
                ExcelRow row = worksheet.Row(curRow);
                double rowHeight = row.Height;
                ret += row.Height;
        
                if (FillMapCustomHeights)
                {
                    bookmarkRowHeights.Add(curRow - fromRow + 1, rowHeight);
                }
            }
        
            if (FillMapCustomHeights && bookmarkRowHeights.Any())
            {
                RowFixedHeights.Add(Bookmark, bookmarkRowHeights);
            }
        
            return ret;
        }

        public double GetNamedRangeHeightWrapped(string _bookmark, string _value, bool _forceObtainFont = false)
        {
            Size size = new(GetNamedRangeWidth(_bookmark), 100);
            Font font = GetNamedRangeFont(_bookmark, _forceObtainFont);
        
            InitGraphics();

            double dpi = Graphics.DpiY;

            double pixels2points = pointsPerInch / dpi;
        
            size = TextRenderer.MeasureText(_value, font, size, TextFormatFlags.WordBreak | TextFormatFlags.TextBoxControl);
            double height = size.Height;
        
            height = Math.Truncate(height * pixels2points) + 3;
            height = height * 100 / (pixels2points * 100) * pixels2points + ((height * 100 % (pixels2points * 100)) == 0 ? pixels2points : 0);
        
            return height;
        }

        public int GetNamedRangeWidth(string Bookmark, bool FillMapCustomWidths = false)
        {
            double                 ret = 0;
            Dictionary<int, double> bookmarkColWidths = null;

            var (worksheetName, _, fromCol, _, toCol) = DefinedNames[Bookmark];

            if (FillMapCustomWidths)
            {
                if (ColFixedWidths == null)
                {
                    ColFixedWidths = new Dictionary<string, Dictionary<int, double>>();
                }
                if (ColFixedWidths.ContainsKey(Bookmark))
                {
                    FillMapCustomWidths = false;
                }
                else
                {
                    bookmarkColWidths = new Dictionary<int, double>();
                }
            }

            ExcelWorksheet worksheet = GetWorksheet(worksheetName);
            for (int curCol = fromCol; curCol <= toCol; curCol++)
            {
                ExcelColumn column = worksheet.Column(curCol);
                ret += (double)Math.Truncate((256.0 * column.Width + (double)Math.Truncate(128 / MaximumDigitWidth)) / 256 * (double)MaximumDigitWidth);
        
                if (FillMapCustomWidths)
                {
                    bookmarkColWidths.Add(curCol - fromCol + 1, column.Width);
                }
            }
        
            if (FillMapCustomWidths && bookmarkColWidths.Any())
            {
                ColFixedWidths.Add(Bookmark, bookmarkColWidths);
            }
        
            return (int)ret;
        }

        public double GetPageHeight(int WorksheetId, bool InPoints = true)
        {
            ExcelWorksheet worksheet = GetWorksheet(WorksheetId);
            var (pageHeight, _) = GetPaperSize(worksheet);
            pageHeight = pageHeight - worksheet.PrinterSettings.TopMargin - worksheet.PrinterSettings.BottomMargin;
        
            if (InPoints)
            {
                pageHeight = Math.Truncate(pageHeight * pointsPerInch);
                pageHeight -= 10;
            }
            return (double)pageHeight;
        }

        public double GetPageWidth(int WorksheetId, bool InPoints = true)
        {
            ExcelWorksheet worksheet = GetWorksheet(WorksheetId);
            var (_, pageWidth) = GetPaperSize(worksheet);
            pageWidth = pageWidth - worksheet.PrinterSettings.LeftMargin - worksheet.PrinterSettings.RightMargin;
        
            if (InPoints)
            {
                InitGraphics();
                decimal dpi = (decimal)Graphics.DpiY;
                pageWidth = Math.Truncate(pageWidth * dpi);
                pageWidth -= 10;
            }
            return (double)pageWidth;
        }

        private static (decimal,decimal) GetPaperSize(ExcelWorksheet  _workSheet)
        {
            decimal width, height;
            bool portrait = true;

            if (_workSheet.PrinterSettings.Orientation == eOrientation.Landscape)
            {
                portrait = false;
            }

            switch (_workSheet.PrinterSettings.PaperSize)
            {
                case ePaperSize.Letter:
                    width = 8.5M;
                    height = 11;
                    break;
                case ePaperSize.LetterSmall:
                    width = 8.5M;
                    height = 11;
                    break;
                case ePaperSize.Tabloid:
                    width = 11;
                    height = 17;
                    break;
                case ePaperSize.Ledger:
                    width = 17;
                    height = 11;
                    break;
                case ePaperSize.Legal:
                    width = 8.5M;
                    height = 14;
                    break;
                case ePaperSize.Statement:
                    width = 5.5M;
                    height = 8.5M;
                    break;
                case ePaperSize.Executive:
                    width = 7.25M;
                    height = 10.5M;
                    break;
                case ePaperSize.A3:
                    width = 11.692913M;
                    height = 16.535433M;
                    break;
                case ePaperSize.A4:
                    width = 8.267716M;
                    height = 11.692913M;
                    break;
                case ePaperSize.A4Small:
                    width = 8.267716M;
                    height = 11.692913M;
                    break;
                case ePaperSize.A5:
                    width = 5.826771M;
                    height = 8.267716M;
                    break;
                case ePaperSize.B4:
                    width = 9.842519M;
                    height = 13.897637M;
                    break;
                case ePaperSize.B5:
                    width = 6.929133M;
                    height = 9.842519M;
                    break;
                case ePaperSize.Folio:
                    width = 8.5M;
                    height = 13;
                    break;
                case ePaperSize.Quarto:
                    width = 8.464566M;
                    height = 10.826771M;
                    break;
                case ePaperSize.Standard11_17:
                    width = 11;
                    height = 17;
                    break;
                case ePaperSize.Note:
                    width = 8.5M;
                    height = 11;
                    break;
                case ePaperSize.Envelope9:
                    width = 3.875M;
                    height = 8.875M;
                    break;
                case ePaperSize.Envelope10:
                    width = 4.125M;
                    height = 9.5M;
                    break;
                case ePaperSize.Envelope11:
                    width = 4.5M;
                    height = 10.375M;
                    break;
                case ePaperSize.Envelope12:
                    width = 4.75M;
                    height = 11;
                    break;
                case ePaperSize.Envelope14:
                    width = 5;
                    height = 11.5M;
                    break;
                case ePaperSize.C:
                    width = 17;
                    height = 22;
                    break;
                case ePaperSize.D:
                    width = 22;
                    height = 34;
                    break;
                case ePaperSize.E:
                    width = 34;
                    height = 44;
                    break;
                case ePaperSize.DLEnvelope:
                    width = 4.330708M;
                    height = 8.661417M;
                    break;
                case ePaperSize.C5Envelope:
                    width = 6.377952M;
                    height = 9.015748M;
                    break;
                case ePaperSize.C3Envelope:
                    width = 12.755905M;
                    height = 18.031496M;
                    break;
                case ePaperSize.C4Envelope:
                    width = 9.015748M;
                    height = 12.755905M;
                    break;
                case ePaperSize.C6Envelope:
                    width = 4.488188M;
                    height = 6.377952M;
                    break;
                case ePaperSize.C65Envelope:
                    width = 4.488188M;
                    height = 9.015748M;
                    break;
                case ePaperSize.B4Envelope:
                    width = 9.842519M;
                    height = 13.897637M;
                    break;
                case ePaperSize.B5Envelope:
                    width = 6.929133M;
                    height = 9.842519M;
                    break;
                case ePaperSize.B6Envelope:
                    width = 6.929133M;
                    height = 4.921259M;
                    break;
                case ePaperSize.ItalyEnvelope:
                    width = 4.330708M;
                    height = 9.055118M;
                    break;
                case ePaperSize.MonarchEnvelope:
                    width = 3.875M;
                    height = 7.5M;
                    break;
                case ePaperSize.Six3_4Envelope:
                    width = 3.625M;
                    height = 6.5M;
                    break;
                case ePaperSize.USStandard:
                    width = 14.875M;
                    height = 11;
                    break;
                case ePaperSize.GermanStandard:
                    width = 8.5M;
                    height = 12;
                    break;
                case ePaperSize.GermanLegal:
                    width = 8.5M;
                    height = 13;
                    break;
                case ePaperSize.ISOB4:
                    width = 9.842519M;
                    height = 13.897637M;
                    break;
                case ePaperSize.JapaneseDoublePostcard:
                    width = 7.874015M;
                    height = 5.826771M;
                    break;
                case ePaperSize.Standard9:
                    width = 9;
                    height = 11;
                    break;
                case ePaperSize.Standard10:
                    width = 10;
                    height = 11;
                    break;
                case ePaperSize.Standard15:
                    width = 15;
                    height = 11;
                    break;
                case ePaperSize.InviteEnvelope:
                    width = 8.661417M;
                    height = 8.661417M;
                    break;
                case ePaperSize.LetterExtra:
                    width = 9.275M;
                    height = 12;
                    break;
                case ePaperSize.LegalExtra:
                    width = 9.275M;
                    height = 15;
                    break;
                case ePaperSize.TabloidExtra:
                    width = 11.69M;
                    height = 18;
                    break;
                case ePaperSize.A4Extra:
                    width = 9.291338M;
                    height = 12.677165M;
                    break;
                case ePaperSize.LetterTransverse:
                    width = 8.275M;
                    height = 11;
                    break;
                case ePaperSize.A4Transverse:
                    width = 8.267716M;
                    height = 11.692913M;
                    break;
                case ePaperSize.LetterExtraTransverse:
                    width = 9.275M;
                    height = 12;
                    break;
                case ePaperSize.SuperA:
                    width = 8.937007M;
                    height = 14.015748M;
                    break;
                case ePaperSize.SuperB:
                    width = 12.007874M;
                    height = 19.173228M;
                    break;
                case ePaperSize.LetterPlus:
                    width = 8.5M;
                    height = 12.69M;
                    break;
                case ePaperSize.A4Plus:
                    width = 8.267716M;
                    height = 12.992125M;
                    break;
                case ePaperSize.A5Transverse:
                    width = 5.826771M;
                    height = 8.267716M;
                    break;
                case ePaperSize.JISB5Transverse:
                    width = 7.165354M;
                    height = 10.118110M;
                    break;
                case ePaperSize.A3Extra:
                    width = 12.677165M;
                    height = 17.519685M;
                    break;
                case ePaperSize.A5Extra:
                    width = 6.850393M;
                    height = 9.251968M;
                    break;
                case ePaperSize.ISOB5:
                    width = 7.913385M;
                    height = 10.866141M;
                    break;
                case ePaperSize.A2:
                    width = 16.535433M;
                    height = 23.385826M;
                    break;
                case ePaperSize.A3Transverse:
                    width = 11.692913M;
                    height = 16.535433M;
                    break;
                case ePaperSize.A3ExtraTransverse:
                    width = 12.677165M;
                    height = 17.519685M;
                    break;
                case ePaperSize.Standard10_14:
                    width  = 10;
                    height = 14;
                    break;
                default: throw new ArgumentException(Resources.UnsupportedFormat);
            }

            if (!portrait)
            {
                (height, width) = (width, height);
            }
            return (height, width);
        }
        public double GetRowHeight(int Row, dynamic WorkSheet) => GetWorksheet(WorkSheet).Row(Row).Height;

        public string GetValueRowCol(int Row, int Col, dynamic WorkSheet) => GetWorksheet(WorkSheet).Cells[Row, Col].Text;

        public string GetValueRowColSetFormatting(int Row, int Col, dynamic WorkSheet)
        {
            ExcelWorksheet workSheet = GetWorksheet(WorkSheet);
            ExcelRange cells =  workSheet.Cells;
            cells.Style.Numberformat.Format = "@";
            return cells[Row, Col].Text;
        }
        
        public ExcelWorksheet GetWorksheet(int id) => _workbook.Worksheets[id];

        public ExcelWorksheet GetWorksheet(string name) => _workbook.Worksheets[name];

        public void HideColumn(int ColumnIdx, bool Hide, dynamic WorksheetId) => GetWorksheet(WorksheetId).Column(ColumnIdx).Hidden(Hide);

        private Dictionary<string, (string, int, int, int, int)> GetDefinedNamesMap()
        {
            ExcelNamedRangeCollection definedNames = _workbook.Names;

            if (definedNames == null || !definedNames.Any())
            {
                return new Dictionary<string, (string, int, int, int, int)>();
            }

            Dictionary<(string, string), (int, int, int, int)> mergedCellsMap = GetMergedCells();

            var definedNamesMap = new Dictionary<string, (string, int, int, int, int)>();

            foreach(ExcelNamedRange definedName in definedNames)
            {
                string reference = definedName.Text;
                if (reference == missingRef)
                {
                    continue;
                }

                string definedNameName = definedName.Name;
                string workSheetName = definedName.Worksheet.Name;

                ExcelCellAddress definedNameAddressStart = definedName.Start;
                int fromRow = definedNameAddressStart.Row;
                int fromCol = definedNameAddressStart.Column;

                ExcelCellAddress definedNameAddressEnd = definedName.End;
                int toRow = definedNameAddressEnd.Row;
                int toCol = definedNameAddressEnd.Column;
        
                if (fromRow == toRow && fromCol == toCol)
                {
                    if (mergedCellsMap.ContainsKey((definedNameAddressStart.Address, workSheetName)))
                    {
                        (fromRow, fromCol, toRow, toCol) = mergedCellsMap[(definedNameAddressStart.Address, workSheetName)];
                    }
                }
        
                definedNamesMap.Add(definedNameName, (workSheetName, fromRow, fromCol, toRow, toCol));
            }

            return definedNamesMap;
        }

        private void InitGraphics()
        {
            if (Graphics == null)
            {
                Control control = new();
                Graphics = control.CreateGraphics();
            }
        }

        public void InsertFormula(string Bookmark, string Formula)
        {
            var (worksheetName, fromRow, fromCol, _, _) = DefinedNames[Bookmark];

            InsertFormulaRowCol(fromRow, fromCol, Formula, worksheetName);
        }

        public void InsertFormulaR1C1RowCol(int Row, int Col, string Formula, dynamic WorkSheet) => GetWorksheet(WorkSheet).Cells[Row, Col].FormulaR1C1 = Formula;

        public void InsertFormulaRow(string Bookmark, string[] WithFormulas)
        {
            var (worksheetName, fromRow, fromCol, _, _) = DefinedNames[Bookmark];

            ExcelRange cells = GetWorksheet(worksheetName).Cells;
            ExcelRange cell = cells[fromRow, fromCol];
        
            foreach(string formula in WithFormulas)
            {
                cell.Formula = formula;
                fromCol++;
                cell = cells[fromRow, fromCol];
            }
        }

        public void InsertFormulaRowCol(int Row, int Col, string Formula, dynamic WorkSheet, bool SupplementFormula = false)
        {
            ExcelRange cell = GetWorksheet(WorkSheet).Cells[Row, Col];
            cell.Formula = SupplementFormula ? cell.Formula + Formula : Formula;
        }
  
        public void InsertPageBreak(int RowAfter, int WorkSheetId)
        {        
            GetWorksheet(WorkSheetId).Row(RowAfter).PageBreak = true;
        
            if (PageBreakRowMap == null)
            {
                PageBreakRowMap = new Dictionary<(int, int), int>
                {
                    { (CurrentPageNumRow, CurrentWorksheetNum), 1 }
                };
            }
            CurrentPageNumRow++;
            PageBreakRowMap.Add((CurrentPageNumRow, CurrentWorksheetNum), RowAfter + 1);
        }

        public void InsertPageBreakCol(int ColAfter, int WorkSheetId)
        {
            GetWorksheet(WorkSheetId).Column(ColAfter).PageBreak = true;
        
            if (PageBreakColMap == null)
            {
                PageBreakColMap = new Dictionary<(int, int), int>
                {
                    { (CurrentPageNumCol, CurrentWorksheetNum), 1 }
                };
            }
            CurrentPageNumCol++;
            PageBreakColMap.Add((CurrentPageNumCol, CurrentWorksheetNum), ColAfter + 1);
        }

        public void InsertPictureRowCol(int Row, int Col, Bitmap Image, dynamic WorkSheet)
        {        
            if (Image != null)
            {
                ExcelPicture pic = GetWorksheet(WorkSheet).Drawings.AddPicture($"imageR{Row}C{Row}", Image);
                pic.SetPosition(Row - 1, 0, Col - 1, 0);
                pic.SetSize(100);
                pic.EditAs = eEditAs.TwoCell;
            }
        }

        public void InsertRow(int Row, int WorkSheetNum, int RowCnt = 1, int CopyStylesFromRow = 0)
        {
            ExcelWorksheet workSheet = GetWorksheet(WorkSheetNum);

            if (CopyStylesFromRow != 0)
            {
                workSheet.InsertRow(Row, RowCnt, CopyStylesFromRow);
            }
            else
            {
                workSheet.InsertRow(Row, RowCnt);
            }
        }

        public int InsertRowsByBookmark(string Bookmark, string DestinationAddr, double Height = 0, double Width = 0)
        {        
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            ExcelRange rangeFrom = GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol];

            ExcelRange rangeTo = CurrentWorkSheetCells[DestinationAddr];
            ExcelCellAddress cellAddressTo = rangeTo.Start;
            int destinationRow = cellAddressTo.Row;
            int destinationCol = cellAddressTo.Column;
        
            rangeFrom.Copy(rangeTo);
        
            if (Height != 0 && RowFixedHeights != null)
            {
                if (RowFixedHeights.ContainsKey(Bookmark))
                {
                    foreach(var keyValue in RowFixedHeights[Bookmark])
                    {
                        int row = destinationRow + keyValue.Key - 1;
                        ExcelRow excelRow = CurrentWorksheet.Row(row);
                        excelRow.Height = keyValue.Value;
                    }
                }
            }
            if (Height != 0 && fromRow == toRow)
            {
                ExcelRow excelRow = CurrentWorksheet.Row(destinationRow);
                excelRow.Height = Height;
            }
        
            if (Width != 0 && ColFixedWidths != null)
            {
                if (ColFixedWidths.ContainsKey(Bookmark))
                {
                    foreach (var keyValue in ColFixedWidths[Bookmark])
                    {
                        int col = destinationCol + keyValue.Key - 1;
                        ExcelColumn excelCol = CurrentWorksheet.Column(col);
                        excelCol.Width = keyValue.Value;
                    }
                }
            }
            if (Width != 0 && fromCol == toCol)
            {
                ExcelColumn excelCol = CurrentWorksheet.Column(destinationCol);
                excelCol.Width = Width;
            }
        
            return toRow - fromRow + 1;
        }

        public void InsertValue(string Bookmark, dynamic AnyVal)
        {
            var (worksheetName, fromRow, fromCol, _, _) = DefinedNames[Bookmark];

            InsertValueRowCol(fromRow, fromCol, AnyVal, worksheetName);
        }

        public void InsertValueRowCol(int Row, int Col, dynamic Value, dynamic WorkSheet)
        {
            GetWorksheet(WorkSheet).SetValue(Row, Col, Value);
        }

        public void InsertWorkSheet(string SheetName, int CopyFromSheet = -1, bool CopyOnlyFormat = false, int AfterSheet = -1, bool ClearDrawings = false)
        {
            ExcelWorksheet worksheet;
            ExcelWorksheets workSheets = _workbook.Worksheets;

            if (CopyFromSheet != -1)
            {
                ExcelWorksheet copyWorksheet = GetWorksheet(CopyFromSheet);
                worksheet = workSheets.Add(SheetName, copyWorksheet);
                worksheet.DefaultRowHeight = copyWorksheet.DefaultRowHeight;
                if (CopyOnlyFormat)
                {
                    ExcelRange cells = worksheet.Cells;
                    cells.Clear();
                    if (ClearDrawings)
                    {
                        worksheet.Drawings.Clear();
                    }
                }
            }
            else
            {
                worksheet = workSheets.Add(SheetName);
            }
        
            if (AfterSheet != -1)
            {
                workSheets.MoveAfter(worksheet.Index, AfterSheet);
            }
        }

        public bool IsCellHeightOverflow(string Bookmark, string Value) => GetNamedRangeHeightWrapped(Bookmark, Value, true) > GetNamedRangeHeightFixed(Bookmark);

        private void MergeCells(int FromRow, int FromCol, int ToRow, int ToCol, dynamic WorkSheetId)
        {        
            if (FromRow == ToRow && FromCol == ToCol)
            {
                return;
            }
        
            GetWorksheet(WorkSheetId).Cells[FromRow, FromCol, ToRow, ToCol].Merge = true;
        }

        public void MoveWorksheetToPosition(int PositionFrom, int PositionTo) => _workbook.Worksheets.MoveBefore(PositionFrom, PositionTo);

        public void RemoveRowFromPivotTable(string PivotTableName, dynamic PivotWorkSheetId, string PivotFieldName)
        {
            ExcelWorksheet pivotWorkSheet = GetWorksheet(PivotWorkSheetId);
            ExcelPivotTable pivotTable = pivotWorkSheet.PivotTables[PivotTableName];
            ExcelPivotTableFieldCollection fieldsCollection = pivotTable.Fields;
            ExcelPivotTableRowColumnFieldCollection rowCollection = pivotTable.RowFields;

            rowCollection.Remove(fieldsCollection[PivotFieldName]);
        }

        private void RowAutoFit(int Row, int WorkSheetId) => GetWorksheet(WorkSheetId).Row(Row).CustomHeight = false;

        public int RowCount(int WorkSheetNum) => GetWorksheet(WorkSheetNum).Dimension.End.Row;

        public void SaveAs(Stream Stream) => _package.SaveAs(Stream);

        public void Save() => _package.Save();

        public void SetActiveSheet(int WorkSheetId = 1) => GetWorksheet(WorkSheetId).View.TabSelected = true;

        public void SetColumnWidth(dynamic WorkSheetId, int ColumnIdx, double Width)
        {
            ExcelColumn column = GetWorksheet(WorkSheetId).Column(ColumnIdx);
            column.Width = Width;
            column.AutoFit(Width, Width);
        }

        public void SetCurrentWorkSheet(dynamic IdOrName)
        {
            ExcelWorksheet worksheet = GetWorksheet(IdOrName);

            CurrentWorksheet = worksheet;
            CurrentWorksheetNum = CurrentWorksheet.Index;
            CurrentWorkSheetName = CurrentWorksheet.Name;
            CurrentWorkSheetCells = CurrentWorksheet.Cells;
        
            CurrentPageNumRow = 1;
            CurrentPageNumCol = 1;
        }

        public void SetHiddenWorksheet(dynamic IdOrName, bool Hide) => GetWorksheet(IdOrName).Hidden(Hide ? eWorkSheetHidden.Hidden : eWorkSheetHidden.VeryHidden);

        public void SetNamedRangeFontSize(string Bookmark, float FontSize)
        {       
            var (worksheetName, fromRow, fromCol, _, _) = DefinedNames[Bookmark];
            GetWorksheet(worksheetName).Cells[fromRow, fromCol].Style.Font.Size = FontSize;
        }

        public void SetNamedRangeNumberFormat(string Bookmark, string NumberFormat)
        {        
            var (worksheetName, fromRow, fromCol, toRow, _) = DefinedNames[Bookmark];

            if (fromRow != toRow)
            {
                throw new Exception();
            }
            GetWorksheet(worksheetName).Cells[fromRow, fromCol].Style.Numberformat.Format = NumberFormat;
        }

        public void SetNamedRangeShrinkToFit(string Bookmark, bool ShinkToFit = true)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            if (fromRow != toRow || fromCol != toCol)
            {
                return;
            }
            GetWorksheet(worksheetName).Cells[fromRow, fromCol].Style.ShrinkToFit = ShinkToFit;
        }

        void SetPivotTableDataSource(string PivotTableName, dynamic PivotWorksheetId, string SourceAddress, dynamic SourceWorkSheetId)
        {
            ExcelWorksheet sourceWorkSheet = GetWorksheet(SourceWorkSheetId);
            ExcelRange sourceRange = sourceWorkSheet.Cells[$"{sourceWorkSheet.Name}!{SourceAddress}"];

            ExcelWorksheet pivotWorkSheet = GetWorksheet(PivotWorksheetId);
            pivotWorkSheet.PivotTables[PivotTableName].CacheDefinition.SourceRange = sourceRange;
        }

        public void SetPrintArea(string Range, int WorksheetId)
        {
            ExcelWorksheet worksheet = GetWorksheet(WorksheetId);
            ExcelRange range = worksheet.Cells[Range];
            worksheet.PrinterSettings.PrintArea = range;
        }

        public void SetPrintAreaDefault(int _worksheetId) => SetPrintArea(GetWorksheet(_worksheetId).Dimension.Address, _worksheetId);

        void SetRangeBackgroundColor(string Bookmark, string HtmlColor)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            ExcelFill fill = GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol].Style.Fill;
            fill.PatternType = ExcelFillStyle.Solid;
            fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(HtmlColor));
        }

        void SetRangeFontBold(string Bookmark, bool Bold)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol].Style.Font.Bold = Bold;
        }

        void SetRangeFontColor(string Bookmark, string HtmlColor)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol].Style.Font.Color.SetColor(ColorTranslator.FromHtml(HtmlColor));
        }

        void SetRangeFontUnderline(string Bookmark, bool Underline)
        {
            var (worksheetName, fromRow, fromCol, toRow, toCol) = DefinedNames[Bookmark];

            GetWorksheet(worksheetName).Cells[fromRow, fromCol, toRow, toCol].Style.Font.UnderLine = Underline;
        }

        private void SetRowHeight(int Row, double Height, dynamic WorkSheet) => GetWorksheet(WorkSheet).Row(Row).Height(Height);

        public void SortAllPivotRows(string PivotTableName, dynamic PivotWorkSheetId, eSortType AscDescSorting)
        {
            ExcelPivotTable pivotTable = GetWorksheet(PivotWorkSheetId).PivotTables[PivotTableName];
            ExcelPivotTableRowColumnFieldCollection rowCollection = pivotTable.RowFields;
            for(int i = 0; i <= rowCollection.Count - 1; i++)
            {
                rowCollection[i].Sort = AscDescSorting;
            }
        }

        private void UnMergeCells(int FromRow, int FromCol, int ToRow, int ToCol, dynamic WorkSheet)
        {        
            if (FromRow == ToRow && FromCol == ToCol)
            {
                return;
            }
        
            GetWorksheet(WorkSheet).Cells[FromRow, FromCol, ToRow, ToCol].Merge = false;
        }

        public static string GetCellAddress(int Column, int Row) => $"{ExcelCellAddress.GetColumnLetter(Column)}{Row}";
    }
}
