﻿using Core.Properties;
using System;
using System.Collections.Generic;

namespace Core
{
    public class DefinedNames
    {
        private Dictionary<string, (string, int, int, int, int)> DefinedNamesMap { get; set; }
        internal DefinedNames(Dictionary<string, (string, int, int, int, int)> DefinedNamesMap)
        {
            this.DefinedNamesMap = DefinedNamesMap;
        }

        internal (string, int, int, int, int) this[string index]
        {
            get
            {
                if (!DefinedNamesMap.ContainsKey(index))
                {
                    throw new Exception(string.Format(Resources.Bookmark0NotFounded, index));
                }

                return DefinedNamesMap[index];
            }
        }
    }
}
