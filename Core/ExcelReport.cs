﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using OfficeOpenXml.Table.PivotTable;
using PdfSharpCore.Pdf;
using PdfSharpCore.Pdf.IO;

namespace Core
{
    abstract public class ExcelReport
    {
        private readonly ExcelDocument _document;
        private readonly Dictionary<string, (bool, int, int, bool, double, double)> _mapSections;
        private readonly IEnumerable<string> _templateSheets;
        private double _currentHeight, _pageHeight, _currentWidth, _pageWidth, _rangeHeight, _rangeHeightRow;
        private int _currentRow, _prevRow, _widthCols, _currentCol, _prevCol, _curWorksheetNum;
        private const double minFontSize = 6;

        protected const string headerName = "HEADER";
        protected const string bodyName = "BODY";
        protected const string footerName = "FOOTER";

        public bool NeedCalcSectionWidth { get; set; }

        protected abstract byte[] ReportBlank { get; }
        protected abstract void InitSectionMap();
        protected abstract IEnumerable<string> InitTemplateSheets();
        protected abstract void CreateReport();
        protected abstract double CalcRangeHeight(string Range);
        protected abstract void FillData(string Section);
        public ExcelDocument Document => _document;

        protected ExcelReport()
        {
            _mapSections = new Dictionary<string, (bool, int, int, bool, double, double)>();
            _templateSheets = InitTemplateSheets();
            _document = new ExcelDocument(new MemoryStream(ReportBlank), _templateSheets);
            InitSectionMap();
        }

        protected void AddColumnToPivotTable(string PivotTableName, dynamic PivotWorkSheetId, string PivotFieldName, eSortType AscDescSorting = eSortType.None) => _document.AddColumnToPivotTable(PivotTableName, PivotWorkSheetId, PivotFieldName, AscDescSorting);

        protected void AddRowToPivotTable(string PivotTableName, dynamic PivotWorkSheetId, string PivotFieldName, eSortType AscDescSorting = eSortType.None) => _document.AddRowToPivotTable(PivotTableName, PivotWorkSheetId, PivotFieldName, AscDescSorting);

        protected void AddSection(string Section, bool GetHeight = false)
        {
            bool printSection = true;
            var (_, fromRow, fromCol, toRow, toCol) = _document.DefinedNames[Section];
        
            _mapSections.Add(Section, (printSection, toRow - fromRow + 1, toCol - fromCol + 1, GetHeight, (printSection && !GetHeight) ? CalcSectionHeight(Section) : 0, CalcSectionWidth(Section)));
        }

        public double CalcSectionHeight(string String) => _document.GetNamedRangeHeightFixed(String, true);

        public double CalcSectionWidth(string String) => !NeedCalcSectionWidth ? 0 : _document.GetNamedRangeWidth(String, true);

        protected void DeleteTemplateSheets() => _templateSheets.ToList().ForEach(s => _document.DeleteWorkSheet(s));

        protected void Execute(string Section, bool ByRow = true)
        {        
            var (printSection, rows, cols, getHeight, height, width) = _mapSections[Section];
        
            if (printSection)
            {
                if (getHeight)
                {
                    height = GetRangeHeight(Section);
                }
        
                _prevRow = _currentRow;
                _prevCol = _currentCol;
        
                if (ByRow)
                {
                    _currentRow += rows;
                }
                else
                {
                    _currentCol += cols;
                }
        
                FillData(Section);

                string destAddr = ExcelDocument.GetCellAddress(_prevCol + 1, _prevRow + 1);
                _document.InsertRowsByBookmark(Section, destAddr, getHeight ? height : 0);
        
                if (ByRow)
                {
                    _currentHeight += height;
                }
                else
                {
                    _currentWidth  += width;
                }
            }
        }

        protected double GetRangeHeight(string Range)
        {
            if (_currentRow == 0 || _rangeHeightRow != _currentRow )
            {
                _rangeHeight = CalcRangeHeight(Range);
                _rangeHeightRow = _currentRow;
            }
            return _rangeHeight;
        }
        public void HideColumn(int ColumnIdx, bool Hide, dynamic WorksheetId) => _document.HideColumn(ColumnIdx, Hide, WorksheetId);

        protected void InsertValueToSection(string Bookmark, dynamic Value) => _document.InsertValue(Bookmark, Value);

        protected void InsertValueToSectionNoOverflow(string Bookmark, dynamic Value, float FontSizeReductionStep = -1)
        {
            bool isOverflow;

            float fontSize = _document.GetNamedRangeFontSize(Bookmark);
            do
            {
                isOverflow = _document.IsCellHeightOverflow(Bookmark, Value);
                if (isOverflow)
                {
                    fontSize += FontSizeReductionStep;
                    _document.SetNamedRangeFontSize(Bookmark, fontSize);
                }
            } while (isOverflow && fontSize > minFontSize);
        
            InsertValueToSection(Bookmark, Value);
        }

        protected void InsertWorksheet(string SheetName, int CopyFromSheet = -1, bool CopyOnlyFormat = false, int AfterSheet = -1, bool ClearDrawings = false) => _document.InsertWorkSheet(SheetName, CopyFromSheet, CopyOnlyFormat, AfterSheet, ClearDrawings);

        protected void RemoveRowFromPivotTable(string PivotTableName, dynamic PivotWorkSheetId, string PivotFieldName) => _document.RemoveRowFromPivotTable(PivotTableName, PivotWorkSheetId, PivotFieldName);

        public Stream CreateDocument()
        {
            CreateReport();
            DeleteTemplateSheets();
            _document.SetActiveSheet(0);
            return _document.CloseDocument(true);
        }

        public int SectionRowsCount(string Section)
        {
            var (_,count,_,_,_,_) = _mapSections[Section];
            return count;
        }

        public void SetColumnWidth(dynamic PivotWorkSheetId, int ColumnIdx, double Width) => _document.SetColumnWidth(PivotWorkSheetId, ColumnIdx, Width);

        protected void SetCurrentWorksheetNum(dynamic WorkSheetId, int WidthInColumns = 0)
        {
            _document.SetCurrentWorkSheet(WorkSheetId);
        
            _pageHeight  = _document.GetPageHeight(_document.CurrentWorksheetNum);
            _pageWidth   = _document.GetPageWidth(_document.CurrentWorksheetNum);

            _curWorksheetNum = _document.CurrentWorksheetNum;
            _currentRow      = 0;
            _currentHeight   = 0;
            _currentCol      = 0;
            _currentWidth    = 0;
            _widthCols = WidthInColumns;
        }

        protected void SetPrintArea()
        {
            if (_widthCols != 0)
            {
                _document.SetPrintArea($"{ExcelDocument.GetCellAddress(1, 1)}:{ExcelDocument.GetCellAddress(_widthCols, _currentRow)}", _curWorksheetNum);
            }
            else
            {
                _document.SetPrintAreaDefault(_curWorksheetNum);
            }
        }

        protected void SortAllPivotRows(string PivotTableName, dynamic PivotWorkSheetId, eSortType AscDescSorting = eSortType.None) => _document.SortAllPivotRows(PivotTableName, PivotWorkSheetId, AscDescSorting);

        public static void MergePDF(Stream FromFile, Stream ToFile) // это тот файл, куда добавляться листы должны
        {
            PdfPages pdfPages;

            PdfDocument inputPDFDocument = PdfReader.Open(FromFile, PdfDocumentOpenMode.Import);
            PdfDocument outputPDFDocument = PdfReader.Open(ToFile, PdfDocumentOpenMode.Modify);
            int pageCount = inputPDFDocument.PageCount;
            pdfPages = inputPDFDocument.Pages;
            if (pageCount > 0)
            {
                for (int i = 0 ; i < pageCount; i++)
                {
                    PdfPage pdfPage = pdfPages[i];
                    outputPDFDocument.AddPage(pdfPage);
                }
            }
        
            outputPDFDocument.Save(ToFile);
        }
    }
}
