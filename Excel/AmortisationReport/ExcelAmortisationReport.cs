﻿using System;
using System.Collections.Generic;
using Core;
using Reports.Properties;


namespace Reports
{
    internal class ExcelAmortisationReport : ExcelReport
    {
        private readonly AmortisationReportDataModel _amortisationReportDataModel;
        private AmortisationReportDataRowModel _amortisationReportDataRowModel;
        private double _totalAmount;
        public ExcelAmortisationReport(AmortisationReportDataModel amortisationReportDataModel) : base()
        {
            _amortisationReportDataModel = amortisationReportDataModel;
        }

        protected override byte[] ReportBlank => Resources.AmortisationAct;

        protected override double CalcRangeHeight(string Range)
        {
            throw new NotImplementedException();
        }

        protected override void CreateReport()
        {
            InsertWorksheet("Report", 0);
            SetCurrentWorksheetNum("Report");
            Execute(headerName);
            foreach (AmortisationReportDataRowModel amortisationReportDataRowModel in _amortisationReportDataModel.amortisationReportDataRowModels)
            {
                _amortisationReportDataRowModel = amortisationReportDataRowModel;
                Execute(bodyName);
                _totalAmount += _amortisationReportDataRowModel.Price * _amortisationReportDataRowModel.Qty;
            }
            Execute(footerName);
        }

        protected override void FillData(string Section)
        {
            switch(Section)
            {
                case headerName:
                    FillHeader();
                    break;
                case bodyName:
                    FillBody();
                    break;
                case footerName:
                    FillFooter();
                    break;
                default:
                    break;
            }
        }

        private void FillHeader()
        {
            InsertValueToSection("ActNumber", _amortisationReportDataModel.ActNumber);
            InsertValueToSection("AmortisationDate", _amortisationReportDataModel.AmortisationDate.Day);
            InsertValueToSection("AmortisationMonth", _amortisationReportDataModel.AmortisationDate.Month);
            InsertValueToSection("AmortisationYear", _amortisationReportDataModel.AmortisationDate.Year);
            InsertValueToSection("StructuralSubdivision", _amortisationReportDataModel.StructuralSubdivision);
            InsertValueToSection("ResponsiblePerson", _amortisationReportDataModel.ResponsiblePerson);
            InsertValueToSection("CommissionComposed", string.Format("Председатель комиссии - заместитель директора {0}, члены комиссии - главный бухгалтер - {1}, гл. инженер - {2}, специалист по пожарной безопасности - {3}, ведущий инженер {4}",
                                                                _amortisationReportDataModel.VicePresident,
                                                                _amortisationReportDataModel.ChiefAccountant,
                                                                _amortisationReportDataModel.ChiefEngineer,
                                                                _amortisationReportDataModel.FireSafetySpecialist,
                                                                _amortisationReportDataModel.LeadEngineer));
        }

        private void FillBody()
        {
            InsertValueToSection("MaterialName", _amortisationReportDataRowModel.MaterialName);
            InsertValueToSection("MaterialCode", _amortisationReportDataRowModel.MaterialCode);
            InsertValueToSection("Unit", _amortisationReportDataRowModel.Unit);
            InsertValueToSection("ConsumptionRate", _amortisationReportDataRowModel.ConsumptionRate);
            InsertValueToSection("Qty", _amortisationReportDataRowModel.Qty);
            InsertValueToSection("Price", _amortisationReportDataRowModel.Price);
            InsertValueToSection("Amount", _amortisationReportDataRowModel.Price * _amortisationReportDataRowModel.Qty);
            InsertValueToSection("Reason", _amortisationReportDataRowModel.Reason);
        }

        private void FillFooter()
        {
            InsertValueToSection("StringAmount", _totalAmount);
            InsertValueToSection("TotalAmount", _totalAmount);
            InsertValueToSection("TotalAmount2", _totalAmount);
            InsertValueToSection("ChiefAccountant", _amortisationReportDataModel.ChiefAccountant);
            InsertValueToSection("ChiefEngineer", _amortisationReportDataModel.ChiefEngineer);
            InsertValueToSection("FireSafetySpecialist", _amortisationReportDataModel.FireSafetySpecialist);
            InsertValueToSection("LeadEngineer", _amortisationReportDataModel.LeadEngineer);
            InsertValueToSection("VicePresident", _amortisationReportDataModel.VicePresident);
        }

        protected override void InitSectionMap()
        {
            AddSection(headerName);
            AddSection(bodyName);
            AddSection(footerName);
        }

        protected override IEnumerable<string> InitTemplateSheets() => new List<string>() { "Sheet1" };
    }
}
