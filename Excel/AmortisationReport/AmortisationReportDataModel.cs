﻿using System;
using System.Collections.Generic;

namespace Reports
{
    internal class AmortisationReportDataModel
    {
        public string ActNumber = "1";
        public DateTime AmortisationDate = DateTime.Now;
        public string StructuralSubdivision = "БОООООЛЬШОЕ ПОДРАЗДЕЛЕНИЕ";
        public string ChiefAccountant = "Иванов И.И.";
        public string ChiefEngineer = "Федоров Ф.Ф.";
        public string FireSafetySpecialist = "Макрова М.М.";
        public string LeadEngineer = "Омайгадовна М.Г.";
        public string ResponsiblePerson = "Респов О.В.";
        public string VicePresident = "Гудов Ю.Ю.";

        public List<AmortisationReportDataRowModel> amortisationReportDataRowModels = new()
        {
            new AmortisationReportDataRowModel()
            {
                MaterialName = "Первый",
                MaterialCode = "001",
                Unit = "шт.",
                ConsumptionRate = "2 шт/ч",
                Qty = 1,
                Price = 10,
                Reason = "Мне"
            },
            new AmortisationReportDataRowModel()
            {
                MaterialName = "Второй",
                MaterialCode = "002",
                Unit = "шт.",
                ConsumptionRate = "15 шт/ч",
                Qty = 2,
                Price = 20,
                Reason = "Просто"
            },
            new AmortisationReportDataRowModel()
            {
                MaterialName = "Третий",
                MaterialCode = "003",
                Unit = "шт.",
                ConsumptionRate = "10 шт/ч",
                Qty = 3,
                Price = 30,
                Reason = "Захотелось"
            },
            new AmortisationReportDataRowModel()
            {
                MaterialName = "Четвертый",
                MaterialCode = "004",
                Unit = "шт.",
                ConsumptionRate = "2шт/ч",
                Qty = 4,
                Price = 40,
                Reason = "Списать"
            },
            new AmortisationReportDataRowModel()
            {
                MaterialName = "Пятый",
                MaterialCode = "005",
                Unit = "шт.",
                ConsumptionRate = "13 шт/ч",
                Qty = 5,
                Price = 50,
                Reason = "Этот"
            },
            new AmortisationReportDataRowModel()
            {
                MaterialName = "Шестой",
                MaterialCode = "006",
                Unit = "шт.",
                ConsumptionRate = "18 шт/ч",
                Qty = 6,
                Price = 60,
                Reason = "Материал"
            },
        };
    }

    internal class AmortisationReportDataRowModel
    {
        public string MaterialName;
        public string MaterialCode;
        public string Unit;
        public string ConsumptionRate;
        public int Qty;
        public double Price;
        public string Reason;
    }
}
