﻿using System;
using System.IO;

namespace Reports
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using (FileStream outputFileStream = new(@"C:\Temp\Done.xlsx", FileMode.Create))
            {
                DefectiveReport report = new(new DefectiveReportData());
                report.CreateDocument().CopyTo(outputFileStream);
            }
            Console.WriteLine("Hello World!");
        }
    }
}
