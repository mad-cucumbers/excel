﻿using Core;
using Reports.Properties;
using System;
using System.Collections.Generic;

namespace Reports
{
    public class DefectiveReport : ExcelReport
    {
        private readonly DefectiveReportData _defectiveReportData;
        private Employee _employee;
        private const string _footer1 = "FOOTER1", _footer2 = "FOOTER2", _footer3 = "FOOTER3";

        public DefectiveReport(DefectiveReportData defectiveReportData) : base()
        {
            _defectiveReportData = defectiveReportData;
        }

        protected override byte[] ReportBlank => Resources.DefectiveList;

        protected override double CalcRangeHeight(string Range)
        {
            throw new NotImplementedException();
        }

        protected override void CreateReport()
        {
            InsertWorksheet("Report", 0);
            SetCurrentWorksheetNum("Report");

            Execute(headerName);
            Execute(_footer1);
            foreach (Employee employee in _defectiveReportData.employees)
            {
                _employee = employee;
                Execute(_footer2);
            }
            Execute(_footer3);
        }

        protected override void FillData(string Section)
        {
            switch (Section)
            {
                case headerName:
                    FillHeader();
                    break;
                case _footer2:
                    FillFooter2();
                    break;
                case _footer3:
                    FillFooter3();
                    break;
                default:
                    break;
            }
        }

        private void FillHeader()
        {
            InsertValueToSection("DepartmentFRP", _defectiveReportData.DepartmentFRP);
            InsertValueToSection("DuringTechnicalInspection", _defectiveReportData.DuringTechnicalInspection);
            InsertValueToSection("InventoryItemId", _defectiveReportData.InventoryItemId);
            Document.AddRangeRichText("ExpertsHaveEstablished", Resources.ExpertsHaveEstablished, false, false, false, false);
            Document.AddRangeRichText("ExpertsHaveEstablished", _defectiveReportData.ExpertsHaveEstablished, false, false, false, true);
            Document.AddRangeRichText("AndItIsRecommended", Resources.AndItIsRecommended);
            Document.AddRangeRichText("AndItIsRecommended", _defectiveReportData.ExpertsHaveEstablished, false, false, false, true);
        }

        private void FillFooter2()
        {
            InsertValueToSection("WorkerPosition", _employee.Position);
            InsertValueToSection("WorkerName", _employee.Name);
        }

        private void FillFooter3()
        {
            InsertValueToSection("ReportDay", _defectiveReportData.ReportDate.ToString("dd"));
            InsertValueToSection("ReportMonth", _defectiveReportData.ReportDate.ToString("MM"));
            InsertValueToSection("ReportYear", _defectiveReportData.ReportDate.ToString("yy"));
        }

        protected override void InitSectionMap()
        {
            AddSection(headerName);
            AddSection(_footer1);
            AddSection(_footer2);
            AddSection(_footer3);
        }

        protected override IEnumerable<string> InitTemplateSheets() => new List<string>() { "Лист1" };
    }
}
