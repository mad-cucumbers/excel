﻿using System;
using System.Collections.Generic;

namespace Reports
{
    public class DefectiveReportData
    {
        public string DepartmentFRP = "Главное управление всеми управлениями";
        public string DuringTechnicalInspection = "Настолько критически супер важное основное средство, что его осмотр ведут три человека";
        public string InventoryItemId = "001";
        public string ExpertsHaveEstablished = "С этим основным средством все в порядке, оно находится в хорошем состоянии, дополнительных действий не требуется";
        public string AndItIsRecommended = "Оставить данное основное средство как есть";

        public List<Employee> employees = new()
        {
            new Employee()
            {
                Position = "Главный инженер",
                Name = "Норуктаев Г.С."
            },

            new Employee()
            {
                Position = "Инженер побольше",
                Name = "Карцев И.В."
            },

            new Employee()
            {
                Position = "Инженер поменьше",
                Name = "Дорков А.Ф."
            },
        };

        public DateTime ReportDate = DateTime.Now;        
    }

    public class Employee
    {
        public string Position;
        public string Name;
    }
}
