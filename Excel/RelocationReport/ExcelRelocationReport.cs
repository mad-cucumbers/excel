﻿using Core;
using Reports.Properties;
using System;
using System.Collections.Generic;

namespace Reports
{
    public class ExcelRelocationReport : ExcelReport
    {
        private readonly RelocationReportDataModel _relocationReportDataModel;
        private RelocationReportRowDataModel _relocationReportRowDataModel;

        public ExcelRelocationReport(RelocationReportDataModel relocationReportDataModel) : base()
        {
            _relocationReportDataModel = relocationReportDataModel;
        }

        protected override byte[] ReportBlank => Resources.RelocationReport;

        protected override double CalcRangeHeight(string Range)
        {
            throw new NotImplementedException();
        }

        protected override void CreateReport()
        {
            InsertWorksheet("Report", 0);
            SetCurrentWorksheetNum("Report");
            Execute(headerName);
            foreach (RelocationReportRowDataModel relocationReportDataRowModel in _relocationReportDataModel.rows)
            {
                _relocationReportRowDataModel = relocationReportDataRowModel;
                Execute(bodyName);
            }
            Execute(footerName);
        }

        protected override void FillData(string Section)
        {
            switch (Section)
            {
                case headerName:
                    FillHeader();
                    break;
                case bodyName:
                    FillBody();
                    break;
                case footerName:
                    FillFooter();
                    break;
                default:
                    break;
            }
        }

        private void FillHeader()
        {
            InsertValueToSection("InvoiceNumber", _relocationReportDataModel.InvoiceNumber);
            InsertValueToSection("InvoiceDay", _relocationReportDataModel.InvoiceDay);
            InsertValueToSection("InvoiceMonth", _relocationReportDataModel.InvoiceMonth);
            InsertValueToSection("InvoiceYear", _relocationReportDataModel.InvoiceYear);
            InsertValueToSection("Organ", _relocationReportDataModel.Organ);
            InsertValueToSection("SenderDepartment", _relocationReportDataModel.SenderDepartment);
            InsertValueToSection("ReceiptDepartment", _relocationReportDataModel.ReceiptDepartment);
            InsertValueToSection("Reason", _relocationReportDataModel.Reason);
        }

        private void FillBody()
        {
            InsertValueToSection("Name", _relocationReportRowDataModel.Name);
            InsertValueToSection("InventoryItemId", _relocationReportRowDataModel.InventoryItemId);
            InsertValueToSection("UnitName", _relocationReportRowDataModel.UnitName);
            InsertValueToSection("OKEIName", _relocationReportRowDataModel.OkeiCode);
            InsertValueToSection("Price", _relocationReportRowDataModel.Price);
            InsertValueToSection("Qty", _relocationReportRowDataModel.Qty);
            InsertValueToSection("Amount", _relocationReportRowDataModel.Amount);
            InsertValueToSection("Note", _relocationReportRowDataModel.Note);
        }

        private void FillFooter()
        {
            InsertValueToSection("SenderPosition", _relocationReportDataModel.SenderPosition);
            InsertValueToSection("SenderName", _relocationReportDataModel.SenderName);
            InsertValueToSection("ReceiptPosition", _relocationReportDataModel.ReceiptPosition);
            InsertValueToSection("ReceiptName", _relocationReportDataModel.ReceiptName);
            InsertValueToSection("ReportDay", _relocationReportDataModel.ReportDay);
            InsertValueToSection("ReportMonth", _relocationReportDataModel.ReportMonth);
            InsertValueToSection("ReportYear", _relocationReportDataModel.ReportYear);
        }

        protected override void InitSectionMap()
        {
            AddSection(headerName);
            AddSection(bodyName);
            AddSection(footerName);
        }

        protected override IEnumerable<string> InitTemplateSheets() => new List<string>() { "Лист1" };
    }
}
