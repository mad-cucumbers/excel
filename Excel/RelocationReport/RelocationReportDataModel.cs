﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports
{
    public class RelocationReportDataModel
    {
        public string InvoiceNumber = "123";
        public string InvoiceDay = DateTime.Now.ToString("dd");
        public string InvoiceMonth = DateTime.Now.ToString("MM");
        public string InvoiceYear = DateTime.Now.ToString("yy");
        public string Organ = "Рога и копыта";
        public string SenderDepartment = "Подразделение отправляторов";
        public string ReceiptDepartment = "Подразделение получаторов";
        public string Reason = "Очень веcкая причина";
        public List<RelocationReportRowDataModel> rows = new()
        {
            new RelocationReportRowDataModel()
            {
                 Name = "Первый",
                 InventoryItemId = "00001",
                 UnitName = "шт.",
                 OkeiCode = "239586",
                 Price = 150,
                 Qty = 5,
                 Note = "Первый"
            },

            new RelocationReportRowDataModel()
            {
                 Name = "Первый",
                 InventoryItemId = "00001",
                 UnitName = "шт.",
                 OkeiCode = "239586",
                 Price = 150,
                 Qty = 5,
                 Note = "Первый"
            },
            
            new RelocationReportRowDataModel()
            {
                 Name = "Первый",
                 InventoryItemId = "00001",
                 UnitName = "шт.",
                 OkeiCode = "239586",
                 Price = 150,
                 Qty = 5,
                 Note = "Первый"
            },
            
            new RelocationReportRowDataModel()
            {
                 Name = "Первый",
                 InventoryItemId = "00001",
                 UnitName = "шт.",
                 OkeiCode = "239586",
                 Price = 150,
                 Qty = 5,
                 Note = "Первый"
            }, 
            new RelocationReportRowDataModel()
            {
                 Name = "Первый",
                 InventoryItemId = "00001",
                 UnitName = "шт.",
                 OkeiCode = "239586",
                 Price = 150,
                 Qty = 5,
                 Note = "Первый"
            },            
            new RelocationReportRowDataModel()
            {
                 Name = "Первый",
                 InventoryItemId = "00001",
                 UnitName = "шт.",
                 OkeiCode = "239586",
                 Price = 150,
                 Qty = 5,
                 Note = "Первый"
            },

        };

        public string SenderPosition = "Главный отправлятор";
        public string SenderName = "Поркович А.В.";
        public string ReceiptPosition = "Главный получатор";
        public string ReceiptName = "Шихкотанович Д.В";
        public string ReportDay = DateTime.Now.ToString("dd");
        public string ReportMonth = DateTime.Now.ToString("MM");
        public string ReportYear = DateTime.Now.ToString("yy");
    }

    public class RelocationReportRowDataModel
    {
        public string Name;
        public string InventoryItemId;
        public string UnitName;
        public string OkeiCode;
        public double Price;
        public int Qty;
        public double Amount => Price * Qty;
        public string Note;
    }
}
